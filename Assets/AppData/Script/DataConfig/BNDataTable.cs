﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;

#if UNITY_EDITOR
public interface IBNDataTable
{
	void Set (object[] records);

	object[] Get ();

	void Sort ();
}
#endif

#if UNITY_EDITOR
public class BNDataTable<TRecord> : ScriptableObject, IBNDataTable where TRecord : class
#else
public class BNDataTable<TRecord> : ScriptableObject where TRecord : class
#endif
{
	public string sheetId = "1nlxhM89IC-RAWuAKn9r6oLU9KPQNc5Utu5_vsPV9qsM";
	public string gridId = "2134253272";

	[SerializeField]
	public List<TRecord> records = new List<TRecord> ();

	[SerializeField]
	private string primaryKeyName;

	#if UNITY_EDITOR
	private class RecordComparer : IComparer<TRecord>
	{
		private FieldInfo primaryKeyInfo;

		public RecordComparer (string primaryKeyName)
		{
			primaryKeyInfo = typeof(TRecord).GetField (primaryKeyName);
		}

		public int Compare (TRecord r1, TRecord r2)
		{
			object value1 = primaryKeyInfo.GetValue (r1);
			object value2 = primaryKeyInfo.GetValue (r2);

			if (value1 == null && value2 == null)
				return 0;

			if (value1 == null && value2 != null)
				return -1;

			if (value1 != null && value2 == null)
				return 1;

			return ((IComparable)value1).CompareTo (value2);
		}
	}

	private RecordComparer comparer;

	protected void SetKeyAndSort (string _primaryKeyName)
	{
		primaryKeyName = _primaryKeyName;
		comparer = new RecordComparer (primaryKeyName);
		records.Sort (comparer);
	}

	protected void SetKeyName (string _primaryKeyName)
	{
		primaryKeyName = _primaryKeyName;

	}

	protected void SortKeyName (string _sortKey)
	{
		comparer = new RecordComparer (_sortKey);
		records.Sort (comparer);
	}

	public virtual void Sort ()
	{
	}

	public void Set (object[] _records)
	{
		if (_records == null)
			return;

		records.Clear ();

		for (int i = 0; i < _records.Length; i++)
			records.Add (_records [i] as TRecord);
	}

	public object[] Get ()
	{
		List<object> _records = new List<object> ();
		for (int i = 0; i < records.Count; i++)
			_records.Add (records [i]);

		return _records.ToArray ();
	}
	#endif

	private FieldInfo primaryKeyInfo;

	private int BinarySearch (int index, int length, object value)
	{
		if (records == null)
			throw new ArgumentNullException ("array");
		
		if (index < 0)
			throw new ArgumentOutOfRangeException ("index", "index is less than the lower bound of array.");
		
		if (length < 0)
			throw new ArgumentOutOfRangeException ("length", "Value has to be >= 0.");
		
		if (index > records.Count - length)
			throw new ArgumentException ("index and length do not specify a valid range in array.");

		if (primaryKeyInfo == null) {
			primaryKeyInfo = typeof(TRecord).GetField (primaryKeyName);
		}
			

		int iMin = index;
		int iMax = index + length - 1;
		int iCmp = 0;
		try {
			while (iMin <= iMax) {
				int iMid = iMin + ((iMax - iMin) / 2);
				iCmp = ((IComparable)primaryKeyInfo.GetValue (records [iMid])).CompareTo (value);
				if (iCmp == 0)
					return iMid;

				if (iCmp > 0)
					iMax = iMid - 1;
				else
					iMin = iMid + 1;
			}
		} catch (Exception e) {
			throw new InvalidOperationException ("Comparing threw an exception.", e);
		}

		return ~iMin;
	}

	private int NormalSearch (int index, int length, object value)
	{
		if (records == null)
			throw new ArgumentNullException ("array");

		if (index < 0)
			throw new ArgumentOutOfRangeException ("index", "index is less than the lower bound of array.");

		if (length < 0)
			throw new ArgumentOutOfRangeException ("length", "Value has to be >= 0.");

		if (index > records.Count - length)
			throw new ArgumentException ("index and length do not specify a valid range in array.");

		if (primaryKeyInfo == null) {
			primaryKeyInfo = typeof(TRecord).GetField (primaryKeyName);
		}

		int iCmp = 0;
		for (int i = index; i < length; i++) {
			iCmp = ((IComparable)primaryKeyInfo.GetValue (records [i])).CompareTo (value);
			if (iCmp == 0) {
				return i;
			}
		}

		return -1;
	}

	public TRecord GetRecord (object value)
	{
		int index = NormalSearch (0, records.Count, value);

		if (index < 0)
			return null;

		return records [index];
	}

    public int GetRecordIndex(object value)
    {
        return NormalSearch(0, records.Count, value);
    }

	public List<TRecord> GetRecords (object value)
	{
		int index = BinarySearch (0, records.Count, value);

		if (index < 0)
			return null;

		List<TRecord> result = new List<TRecord> ();

		int i = index - 1;
		while (i >= 0 && ((IComparable)primaryKeyInfo.GetValue (records [i])).CompareTo (value) == 0) {
			result.Add (records [i]);
			i--;
		}

		result.Add (records [index]);

		i = index + 1;
		while (i < records.Count && ((IComparable)primaryKeyInfo.GetValue (records [i])).CompareTo (value) == 0) {
			result.Add (records [i]);
			i++;
		}

		return result;
	}
}