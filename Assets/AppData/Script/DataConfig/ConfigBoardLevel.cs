using System;
using UnityEngine;

[Serializable]
public class ConfigBoardLevelRecord
{
    public int width;
    public int height;
}

[CreateAssetMenu(menuName = "Game/ConfigBoardLevel")]
public class ConfigBoardLevel : BNDataTable<ConfigBoardLevelRecord>
{
#if UNITY_EDITOR
    [ContextMenu("Sync")]
    private void Sync()
    {
        //ReadGoogleSheet.FillData<ConfigBoardLevelRecord>("1nlxhM89IC-RAWuAKn9r6oLU9KPQNc5Utu5_vsPV9qsM", "0", lst =>
        //{
        //    records = lst;
        //});
    }

    [ContextMenu("Open Sheet")]
    private void OpenSheet()
    {
        //Application.OpenURL("https://docs.google.com/spreadsheets/d/1nlxhM89IC-RAWuAKn9r6oLU9KPQNc5Utu5_vsPV9qsM/edit#gid=0");
    }
#endif
}
