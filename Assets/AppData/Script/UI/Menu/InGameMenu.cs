using AFramework.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InGameMenu : BaseUIMenu
{
    [SerializeField] Button buttonHome;

    IEnumerator cR_LoadMainMenu;

    private void Awake()
    {
        buttonHome.onClick.AddListener(() =>
        {
            BoardManager.I.RemoveAllNodePieces();

            if (cR_LoadMainMenu != null)
            {
                StopCoroutine(cR_LoadMainMenu);
            }
            cR_LoadMainMenu = CR_LoadMainMenu();
            StartCoroutine(cR_LoadMainMenu);
        });
    }

    public override void Init(object[] initParams)
    {
        base.Init(initParams);

        BoardManager.I.StartGame();
    }

    private IEnumerator CR_LoadMainMenu()
    {
        Resources.UnloadUnusedAssets();
        yield return new WaitForEndOfFrame();

        CanvasManager.PopAllLayer(eUILayer.Menu);
        CanvasManager.Push(GlobalInfo.UIMainMenu, null);

        cR_LoadMainMenu = null;
    }

}
