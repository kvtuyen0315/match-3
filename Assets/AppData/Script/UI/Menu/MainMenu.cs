using AFramework.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : BaseUIMenu
{
    [SerializeField] Button buttonStartGame;

    IEnumerator cR_LoadInGame;

    private void Awake()
    {
        buttonStartGame.onClick.AddListener(() =>
        {
            if (cR_LoadInGame != null)
            {
                StopCoroutine(cR_LoadInGame);
            }
            cR_LoadInGame = CR_LoadInGame();
            StartCoroutine(cR_LoadInGame);
        });
    }

    private IEnumerator CR_LoadInGame()
    {
        Resources.UnloadUnusedAssets();
        yield return new WaitForEndOfFrame();

        CanvasManager.PopAllLayer(eUILayer.Menu);
        CanvasManager.Push(GlobalInfo.UIInGameMenu, null);

        cR_LoadInGame = null;
    }
}
