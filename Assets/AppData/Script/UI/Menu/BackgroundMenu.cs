using AFramework.UI;
using UnityEngine.UI;

public class BackgroundMenu : BaseUIMenu
{
    public static BackgroundMenu I { get; protected set; }

    public Image imgBG;

    private void Awake()
    {
        I = this;
    }

}
