using AFramework.UI;

public class FlashScenePopup : BaseUIMenu
{
    public override void Init(object[] initParams)
    {
        base.Init(initParams);

        Pop();
    }
}
