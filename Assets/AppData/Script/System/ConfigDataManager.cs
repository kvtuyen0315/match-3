using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AFramework;

public class ConfigDataManager : SingletonMono<ConfigDataManager>
{
    public static ConfigBoardLevel configBoardLevel { get; set; }

    public IEnumerator LoadConfigInit(Action callBack)
    {
        configBoardLevel = Resources.Load<ConfigBoardLevel>("DataConfig/ConfigBoardLevel");
        while (!configBoardLevel)
        {
            yield return new WaitForEndOfFrame();
        }

        List<Sprite> listPieces = new List<Sprite>();
        listPieces.AddRange(Resources.LoadAll<Sprite>("GameDatas/Pieces/shapes"));
        listPieces.AddRange(Resources.LoadAll<Sprite>("GameDatas/Pieces/shapes_two"));
        for (int i = 0; i < listPieces.Count; i++)
        {
            GameDatas.piecesData.Add((ePiecesType)i, listPieces[i]);
        }

        GameDatas.nodePiecePrefab = Resources.Load<NodePiece>("GameDatas/Prefabs/NodePiecePrefab");

        yield return new WaitForEndOfFrame();

        callBack?.Invoke();
    }
}
