using AFramework;

public class GlobalInfo : SingletonMono<GlobalInfo>
{
    #region Menu & Popup.
    public const string UIDefaultPath = "UI/";
    public const string UIBackgroundMenu = "Menu/BackgroundMenu";
    public const string UIMainMenu = "Menu/MainMenu";
    public const string UIInGameMenu = "Menu/InGameMenu";

    public const string UIFlashScreenPopup = "Popup/FlashScreenPopup";
    #endregion

    public const string acceptableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdeghijklmnopqrstuvwxyz1234567890!@#$^&*()";


    public int inputBoardLevel;
}
