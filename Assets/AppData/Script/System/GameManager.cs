using AFramework;
using AFramework.UI;

public class GameManager : SingletonMono<GameManager>
{
    private void Start()
    {
        CanvasManager.Init(GlobalInfo.UIDefaultPath, GlobalInfo.UIFlashScreenPopup);
        CanvasManager.Push(GlobalInfo.UIBackgroundMenu, null);

        StartCoroutine(ConfigDataManager.I.LoadConfigInit(() =>
        {
            CanvasManager.Push(GlobalInfo.UIMainMenu, null);
        }));
    }
}
