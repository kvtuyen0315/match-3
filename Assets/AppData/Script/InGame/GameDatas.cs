using AFramework;
using System.Collections.Generic;
using UnityEngine;

public class GameDatas : SingletonMono<GameDatas>
{
    public static Dictionary<ePiecesType, Sprite> piecesData = new Dictionary<ePiecesType, Sprite>();
    public static NodePiece nodePiecePrefab { get; set; }
}