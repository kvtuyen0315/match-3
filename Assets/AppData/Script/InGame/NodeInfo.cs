using System;

[Serializable]
public class NodeInfo
{
    public int value;
    public Point index;

    public NodeInfo(int value, Point index)
    {
        this.value = value;
        this.index = index;
    }
}
