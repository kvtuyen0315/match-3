using System;
using UnityEngine;

[Serializable]
public class Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void Mult(int mult)
    {
        x *= mult;
        y *= mult;
    }

    public void Add(Point point)
    {
        x += point.x;
        y += point.y;
    }

    public Vector2 ToVector()
    {
        return new Vector2(x, y);
    }

    public bool Equals(Point point)
    {
        return x == point.x && y == point.y;
    }

    public static Point FromVector(Vector2 vector)
    {
        return new Point((int)vector.x, (int)vector.y);
    }

    public static Point Mult(Point point, int m)
    {
        return new Point(point.x * m, point.y * m);
    }

    public static Point Add(Point point, Point other)
    {
        return new Point(point.x + other.x, point.y + other.y);
    }

    public static Point Clone(Point point)
    {
        return new Point(point.x, point.y);
    }

    public static Point Zero { get => new Point(0, 0); }
    public static Point One { get => new Point(1, 1); }
    public static Point Up { get => new Point(0, 1); }
    public static Point Down { get => new Point(0, -1); }
    public static Point Right { get => new Point(1, 0); }
    public static Point Left { get => new Point(-1, 0); }

}
