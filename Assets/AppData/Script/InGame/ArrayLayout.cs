using System;
using UnityEngine;

[Serializable]
public class ArrayLayout
{
    [Serializable] public struct RowData { public bool[] row; }
    public Grid grid;
    public RowData[] rows;
}
