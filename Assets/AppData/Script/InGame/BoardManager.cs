using AFramework.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardManager : BaseUIComp
{
    public static BoardManager I { get; protected set; }

    public Transform holderBoard;
    public GridLayoutGroup groupLayer;

    public ArrayLayout boardLayout;
    public NodeInfo[,] boards { get; set; }
    private List<NodePiece> mListNodePieces = new List<NodePiece>();

    System.Random mRandomSeed;

    public ConfigBoardLevelRecord currentBoardLevel { get; set; }

    private void Awake()
    {
        I = this;
    }

    public void StartGame()
    {
        mRandomSeed = new System.Random(GetRadomSeed().GetHashCode());
        currentBoardLevel = ConfigDataManager.configBoardLevel.records[GlobalInfo.I.inputBoardLevel];

        InitializeBoard();
        VerifyBoard();
        if (cR_InstantiateBoard != null)
        {
            StopCoroutine(cR_InstantiateBoard);
        }
        cR_InstantiateBoard = CR_InstantiateBoard();
        StartCoroutine(cR_InstantiateBoard);
    }

    private void InitializeBoard()
    {
        boards = new NodeInfo[currentBoardLevel.width, currentBoardLevel.height];
        for (int column = 0; column < currentBoardLevel.width; column++)
        {
            for (int row = 0; row < currentBoardLevel.height; row++)
            {
                boards[column, row] = new NodeInfo((boardLayout.rows[row].row[column]) ? - 1 : FillPiece(), new Point(column, row));
            }
        }
    }

    private void VerifyBoard()
    {
        List<int> listRemove;
        for (int column = 0; column < currentBoardLevel.width; column++)
        {
            for (int row = 0; row < currentBoardLevel.height; row++)
            {
                Point point = new Point(column, row);
                int value = GetValueAtPoint(point);
                if (value <= 0) continue;

                listRemove = new List<int>();
                while (ListConnected(point, true).Count > 0)
                {
                    value = GetValueAtPoint(point);
                    if (!listRemove.Contains(value))
                    {
                        listRemove.Add(value);
                    }

                    SetValueAtPoint(point, NewValue(ref listRemove));
                }
            }
        }
    }

    IEnumerator cR_InstantiateBoard;
    private IEnumerator CR_InstantiateBoard()
    {
        groupLayer.enabled = true;

        for (int column = 0; column < currentBoardLevel.width; column++)
        {
            for (int row = 0; row < currentBoardLevel.height; row++)
            {
                int value = boards[column, row].value;
                if (value <= 0) continue;

                NodePiece piece = Instantiate(GameDatas.nodePiecePrefab, groupLayer.transform);
                mListNodePieces.Add(piece);
            }
        }

        yield return new WaitForEndOfFrame();
        groupLayer.enabled = false;

        foreach (NodePiece nodePiece in mListNodePieces)
        {
            nodePiece.transform.SetParent(holderBoard);
        }

        cR_InstantiateBoard = null;
    }

    List<Point> ListConnected(Point point, bool main)
    {
        List<Point> listConnected = new List<Point>();
        int value = GetValueAtPoint(point);
        Point[] directions = new Point[]
        {
            Point.Up,
            Point.Right,
            Point.Down,
            Point.Left,
        };

        foreach (Point dir in directions)
        {
            List<Point> line = new List<Point>();

            int same = 0;
            for (int i = 1; i < 3; i++)
            {
                Point check = Point.Add(point, Point.Mult(dir, i));
                if (GetValueAtPoint(check) == value)
                {
                    line.Add(check);
                    same++;
                }
            }

            if (same > 1)
            {
                AddPoints(ref listConnected, line);
            }
        }

        for (int i = 0; i < 2; i++)
        {
            List<Point> line = new List<Point>();

            int same = 0;
            Point[] check = new Point[]
            {
                Point.Add(point, directions[i]),
                Point.Add(point, directions[i + 2])
            };
            foreach (Point next in check)
            {
                if (GetValueAtPoint(next) == value)
                {
                    line.Add(next);
                    same++;
                }
            }

            if (same > 1)
            {
                AddPoints(ref listConnected, line);
            }
        }

        for (int i = 0; i < 4; i++)
        {
            List<Point> square = new List<Point>();

            int same = 0;
            int next = i + 1;
            if (next >= 4) next = 0;

            Point[] check = new Point[]
            {
                Point.Add(point, directions[i]),
                Point.Add(point, directions[next]),
                Point.Add(point, Point.Add(directions[i], directions[next])),
            };
            foreach (Point p in check)
            {
                if (GetValueAtPoint(p) == value)
                {
                    square.Add(point);
                    same++;
                }
            }

            if (same > 2)
            {
                AddPoints(ref listConnected, square);
            }
        }

        if (main)
        {
            for (int i = 0; i < listConnected.Count; i++)
            {
                AddPoints(ref listConnected, this.ListConnected(listConnected[i], false));
            }
        }

        if (listConnected.Count > 0)
        {
            listConnected.Add(point);
        }

        return listConnected;
    }

    private void AddPoints(ref List<Point> listPoints, List<Point> listAdd)
    {
        if (listPoints.Count <= 0) return;

        foreach (Point point in listAdd)
        {
            bool canAdd = true;
            for (int i = 0; i < listAdd.Count; i++)
            {
                if (listPoints[i].Equals(point))
                {
                    canAdd = false;
                    break;
                }
            }

            if (canAdd) point.Add(point);
        }
    }

    private int GetValueAtPoint(Point point)
    {
        if (point.x < 0 || point.x >= currentBoardLevel.width || point.y < 0 || point.y >= currentBoardLevel.height) return -1;
        return boards[point.x, point.y].value;
    }

    private void SetValueAtPoint(Point point, int value)
    {
        boards[point.x, point.y].value = value;
    }

    private int FillPiece()
    {
        int value = (mRandomSeed.Next(0, 100) / (100 / GameDatas.piecesData.Count) + 1);
        return value;
    }

    private string GetRadomSeed()
    {
        string seed = string.Empty;
        for (int i = 0; i < 20; i++)
        {
            seed += GlobalInfo.acceptableChars[UnityEngine.Random.Range(0, GlobalInfo.acceptableChars.Length)];
        }
        return seed;
    }

    int NewValue(ref List<int> listRemove)
    {
        List<int> listAvailable = new List<int>();
        for (int i = 0; i < GameDatas.piecesData.Count; i++)
            listAvailable.Add(i + 1);
        for (int i = 0; i < listRemove.Count; i++)
            listAvailable.Remove(i);

        if (listAvailable.Count <= 0) return 0;
        return listAvailable[mRandomSeed.Next(0, listAvailable.Count)];
    }

    public void RemoveAllNodePieces()
    {
        foreach (NodePiece nodePiece in mListNodePieces)
        {
            Destroy(nodePiece.gameObject);
        }
        mListNodePieces.Clear();
    }
}
